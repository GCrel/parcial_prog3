package Repository;

import models.Book;

import java.util.List;

public interface IQueringAllBooksRepo {
    List<Book> getBooks();
}
