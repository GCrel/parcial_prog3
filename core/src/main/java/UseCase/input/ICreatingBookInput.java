package UseCase.input;

import Exceptions.CreatingBookException;
import models.Book;

public interface ICreatingBookInput {
    boolean creatingBook(Book book) throws CreatingBookException;
}
