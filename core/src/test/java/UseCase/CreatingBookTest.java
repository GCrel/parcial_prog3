package UseCase;

import Exceptions.CreatingBookException;
import Repository.ICreatingBookRepo;
import models.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CreatingBookTest {
    @Mock
    private ICreatingBookRepo creatingBookRepo;

    @Test
    void CreatingBook_successful() {
        CreatingBook creatingBook = new CreatingBook(creatingBookRepo);
        Book book = Book.factory("GCREL",145,1983,"El Principito");
        when(creatingBookRepo.bookExist(book.getAuthor(),book.getTitle())).thenReturn(false);
        when(creatingBookRepo.seveBook(book)).thenReturn(true);
        boolean flag = creatingBook.creatingBook(book);
        Assertions.assertTrue(flag);
    }

    @Test
    void CreatingBook_failed() {
        CreatingBook creatingBook = new CreatingBook(creatingBookRepo);
        Book book = Book.factory("GCREL",145,1983,"El Principito");
        when(creatingBookRepo.bookExist(book.getAuthor(),book.getTitle())).thenReturn(true);
        Assertions.assertThrows(
                CreatingBookException.class,
                () ->creatingBook.creatingBook(book)
        );
    }




}
