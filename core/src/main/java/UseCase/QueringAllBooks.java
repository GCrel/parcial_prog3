package UseCase;

import Repository.IQueringAllBooksRepo;
import UseCase.input.IQueringAllBooksInput;
import models.Book;

import java.util.List;

public class QueringAllBooks implements IQueringAllBooksInput {
    private IQueringAllBooksRepo queringAllBooksRepo;

    public QueringAllBooks(IQueringAllBooksRepo queringAllBooksRepo) {
        this.queringAllBooksRepo = queringAllBooksRepo;
    }

    @Override
    public List<Book> getAllBooks() {
        return queringAllBooksRepo.getBooks();
    }
}
