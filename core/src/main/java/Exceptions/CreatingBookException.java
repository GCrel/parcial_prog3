package Exceptions;

public class CreatingBookException extends RuntimeException{
    public CreatingBookException(String message) {
        super(message);
    }
}
