package Repository;

import models.Book;

public interface ICreatingBookRepo {
    boolean bookExist(String author, String title);
    boolean seveBook(Book book);
}
