package UseCase;

import Repository.IQueringAllBooksRepo;
import models.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class QueringAllBooksTest {
    @Mock
    IQueringAllBooksRepo queringAllBooksRepo;
    @Test
    void QueringAllBooks() {
        QueringAllBooks queringAllBooks = new QueringAllBooks(queringAllBooksRepo);
        Book book1 = Book.factory("GCREL",145,1983,"El Principito");
        Book book2 = Book.factory("Abel",145,1983,"1983");
        Book book3 = Book.factory("emma",145,1983,"Sin Novedad en el Frente");
        Book book4 = Book.factory("Matiaz",145,1983,"OyP");

        ArrayList<Book> listBooksExpected = new ArrayList<Book>();
        listBooksExpected.add(book1);
        listBooksExpected.add(book2);
        listBooksExpected.add(book3);


        when(queringAllBooksRepo.getBooks()).thenReturn(listBooksExpected);
        Assertions.assertTrue(listBooksExpected.equals(queringAllBooks.getAllBooks()));
    }
}
