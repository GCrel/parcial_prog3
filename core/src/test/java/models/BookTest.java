package models;

import Exceptions.BookException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BookTest {
    @Test
    public void Book_factory() {
        Book book = Book.factory("GCREL",145,1983,"El Principito");
        Assertions.assertNotNull(book);
    }

    @Test
    public void Book_checkOut() {
        Book book = Book.factory("GCREL",145,false,1983,"El Principito");
        book.checkOut();
        Assertions.assertTrue(book.isBookStatus());
        book.checkOut();
        Assertions.assertTrue(book.isBookStatus());
    }

    @Test
    public void Book_returnBook() {
        Book book = Book.factory("GCREL",145,1983,"El Principito");
        book.returnBook();
        Assertions.assertFalse(book.isBookStatus());
        book.returnBook();
        Assertions.assertFalse(book.isBookStatus());
    }

    @Test
    public void Book_displayDetails() {
        Book book = Book.factory("GCREL",145,1983,"El Principito");
        String expected= "Title='" + "El Principito" + '\'' +
                ", Author='" + "GCREL" + '\'' +
                ", Year of Publication=" + 1983 +
                ", Number of Pages=" + 145 +
                ", Book Status=" + true;

        Assertions.assertEquals(book.displayDetails(),expected);
    }

    @Test
    public void Book_isLongRead() {
        Book book1 = Book.factory("GCREL",145,1983,"El Principito");
        Book book2 = Book.factory("GCREL",445,1983,"El Principito");
        Assertions.assertTrue(book2.isLongRead());
        Assertions.assertFalse(book1.isLongRead());
    }

    @Test
    public void Book_InvalidTitle() {
        Assertions.assertThrows(
                BookException.class,
                () -> Book.factory("",145,1983,"El Principito")
        );
        Assertions.assertThrows(
                BookException.class,
                () -> Book.factory(null,145,1983,"El Principito")
        );
    }

    @Test
    public void Book_InvalidAuthor () {
        Assertions.assertThrows(
                BookException.class,
                () -> Book.factory("GCREL",145,1983,"")
        );
        Assertions.assertThrows(
                BookException.class,
                () -> Book.factory("GCREL",145,1983,null)
        );
    }

    @Test
    public void Book_InvalidNumberPages () {
        Assertions.assertThrows(
                BookException.class,
                () -> Book.factory("GCREL",0,1983,"El Principito")
        );
        Assertions.assertThrows(
                BookException.class,
                () -> Book.factory("GCREL",-2,1983,"El Principito")
        );
    }
}
