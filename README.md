# Book Service Core Module

This README file outlines the requirements for testing the Book class provided in the Book.java file. The Book class represents a book in a library system with attributes are title, author, year of publication, number of pages, and its checked-out status. The class includes methods for managing the book's state and providing information about it.

## Requirements

1. Factory Method

Instead of using the constructor directly to create instances of the Book class, implement a factory method. This method will encapsulate the object creation process, allowing for additional configuration or validation logic if needed.

2. Unit Tests

Develop unit tests to thoroughly test the methods in the Book class. Use a testing framework like JUnit to create and run your tests.
Key Methods to Test

    checkOut() -> idempotent method
    returnBook() -> idempotent method
    displayDetails() -> any format you decide
    isLongRead() -> (over 200 pages, is a long read book)

3. Use Cases

Create two distinct use cases demonstrating how the Book class can be used in a real-world scenario. Each use case should be accompanied by its own unit tests.
   - Use Case 1: *Creating a Unique Book*
   
        Implement logic to ensure that no two books can have the same title and author combination.
        Attempt to create multiple books, some of which may have the same title and author, and verify that duplicates are not allowed.
   
   - Use Case 2: *Querying All Books*
   
        Create a method to query and list all books.
        Demonstrate this by creating several books and retrieving the list of all created books.

4. Unit Tests for Use Cases

Each use case should have corresponding unit tests to verify the correct functionality. Ensure that the tests validate the expected behavior of the class methods under various conditions.

## Submission
Implement the factory method in the Book class.
Write the unit tests for the Book class and ensure they pass.
Implement the use cases and their respective unit tests.

## Last commit to consider
This test will take into account all commits submitted before 11:00.