package models;

import Exceptions.BookException;

public class Book {
    private String title;
    private String author;
    private int yearPublication;
    private int numberPages;
    private boolean bookStatus;

    private Book(String author, int numberPages, boolean bookStatus ,int yearPublication, String title) {
        this.author = author;
        this.numberPages = numberPages;
        this.yearPublication = yearPublication;
        this.bookStatus = bookStatus;
        this.title = title;
    }

    public static Book factory(String author, int numberPages, int yearPublication, String title) throws BookException{
        if (author == null || author.isBlank())
            throw new BookException("El nombre del Autor no puede estar vacio");
        if (title == null || title.isBlank())
            throw new BookException("El titulo no puede estar vacio");
        if (numberPages <=0)
            throw new BookException("No puede existir libros sin paginas o con paginas negativas");


        return new Book(author,numberPages,true,yearPublication,title);
    }
    public static Book factory(String author, int numberPages, boolean bookStatus, int yearPublication, String title){
        return new Book(author,numberPages,bookStatus,yearPublication,title);
    }

    public void checkOut() {
        this.bookStatus = true;
    }

    public void returnBook() {
        this.bookStatus = false;
    }

    public String displayDetails(){
        return  "Title='" + title + '\'' +
                ", Author='" + author + '\'' +
                ", Year of Publication=" + yearPublication +
                ", Number of Pages=" + numberPages +
                ", Book Status=" + bookStatus;
    }

    public boolean isLongRead() {
        return this.numberPages >200;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getYearPublication() {
        return yearPublication;
    }

    public int getNumberPages() {
        return numberPages;
    }

    public boolean isBookStatus() {
        return bookStatus;
    }
}
