package UseCase;

import Exceptions.CreatingBookException;
import Repository.ICreatingBookRepo;
import UseCase.input.ICreatingBookInput;
import models.Book;

public class CreatingBook implements ICreatingBookInput {
    private ICreatingBookRepo creatingBookRepo;

    public CreatingBook(ICreatingBookRepo creatingBookRepo) {
        this.creatingBookRepo = creatingBookRepo;
    }

    @Override
    public boolean creatingBook(Book book) throws CreatingBookException{
        if (creatingBookRepo.bookExist(book.getAuthor(),book.getTitle())) {
            throw new CreatingBookException("Este Libro ya existe");
        }
        return creatingBookRepo.seveBook(book);
    }
}
