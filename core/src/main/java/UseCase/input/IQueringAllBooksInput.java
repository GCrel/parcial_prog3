package UseCase.input;

import models.Book;

import java.util.List;

public interface IQueringAllBooksInput {
    List<Book> getAllBooks();
}
